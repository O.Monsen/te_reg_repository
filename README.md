# TE_Reg_Repository

---------------------------------------------------

README file: Regulatory divergence between gene duplicates is associated with transposable element derived cis-regulatory elements in liver

Description of project:
Transposable elements are estimated to play important roles the history of genome evolution. A recent analysis revealed that duplicate gene copies which had higher expression in liver following the salmonid whole genome duplication 100 mya~ were coinciding with a higher number of predicted TE-derived cis-regulatory elements. However, the ability of these TE CREs to recruit transcription factors and impact gene expression in vivo is unknown. This project evaluates 11 TEs using luciferase promoter reporter assays in salmon primary liver cells. 

This project has two goals. 1. To determine the last transposition event of each TE, and whether this was before or after the salmonid WGD event. 2. To statistically analyse luciferase reporter assay data, and evaluate if the TEs may have an impact on gene expression. This is done by normalizing the output values from luciferase gene expression (the test variable being effected by TE regulation) by dividing this by a control gene expression (renilla). This reduces the impact of confounding variables such as varying cell count. These results are visualized in bar plots beside a positive control (CMV), and p-values are calculated by performing a regression analysis (lm) for the Non-Mariner dataset, and a Tukey test for the Tc1/Mariner dataset. This is due to that the Tc1/Mariner dataset is not a repeat of the same experiment three times, as is the case with the non/Mariner dataset. Instead, the Tc1/Mariner dataset is three experiments testing different sections of the TE to try to identify the specific motifs/locations involved in up-regulation. 

There are three R scripts in this project. They are named:
- Ridgeplot_Dissimilarity_Pipeline.Rmd
- TE_Tc1_Experiments_Analysis.Rmd
- TE_NonMariners_Experiments_Analysis.Rmd

Ridgeplot_Dissimilarity_Pipeline.Rmd
This script determines the "age" of the TEs. This is done by calculating the similarity, or dissimilarity of the DNA sequences to other similar elements. The more dissimilar the TE is from other similar elements, the older it is estimated to be. 

TE_Tc1_Experiments_Analysis.Rmd
- This script normalizes the luciferase reporter assay results from the Tc1/Mariner TE experiments and returns a bar plot of the normalized data, as well as p-values from a Tukey test of the data compared to the positive control sample (CMV). 

TE_NonMariners_Experiments_Analysis.Rmd
- This script normalizes the luciferase reporter assay results from the Non-Mariner TE experiments and returns a bar plot of the normalized data, as well as p-values from a linear model regression analysis.



