   SW   perc perc perc  query     position in query                 matching            repeat        position in repeat

score   div. del. ins.  sequence  begin     end            (left)   repeat              class/family begin  end    (left)    ID

 

  283   14.7  6.0 10.0  C1           366106    366188 (158672561) + XXX_SsalEle0307     Unspecified    966   1045 (1136)     1

  951    7.7 10.7  2.3  C1           562876    563034 (158475715) C RII_SsalEle0377     Unspecified   (22)   1707   1536     2

1125   16.3  3.2  8.6  C1           563846    564127 (158474622) + XXX_SsalEle1054     Unspecified    154    421    (4)     3

  281   17.0  0.0  0.0  C1           572943    572995 (158465754) + XXX_SsalEle1054     Unspecified    369    421    (4)     4

  235    0.0  0.0  0.0  C1           655786    655812 (158382937) C XXX_SsalEle0307     Unspecified  (297)   1884   1858     5

1547   14.4  8.8  2.5  C1           826426    826731 (158212018) + DTX_SsalEle0849     Unspecified      3    327  (407)     6

1951   11.8  5.0  0.9  C1           827206    827523 (158211226) + DTX_SsalEle0849     Unspecified    287    617  (117)     7
