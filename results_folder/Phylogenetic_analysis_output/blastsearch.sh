#!/bin/bash
#SBATCH --ntasks=4 # 1 core(CPU)
#SBATCH --nodes=1 # Use 1 node
#SBATCH --job-name=blastingmariners # sensible name for the job
#SBATCH --mem=10G #Default memory per CPU is 3GB.
#SBATCH --partition=hugemem # Use the verysmallmem-partition for jobs requiring < 10 GB RAM.
#SBATCH --mail-type=ALL

mkdir blastout

makeblastdb -in RepBase_REPET_nt.fa -dbtype nucl

blastn -query insertions.fa -db RepBase_REPET_nt.fa -out blastout/NTinstoDB.out -evalue 0.01 -outfmt 6 -num_threads 8 -max_target_seqs 5

blastn -query interesting_consensuses -db RepBase_REPET_nt.fa -out blastout/NTconstoDB.out -evalue 0.01 -outfmt 6 -num_threads 8 -max_target_seqs 5
