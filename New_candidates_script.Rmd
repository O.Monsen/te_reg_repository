---
title: "New_Candidates"
author: "Hanna"
date: "3/9/2021"
output: html_document
---

```{r setup, include=FALSE}
r = getOption("repos")
r["CRAN"] = "http://cran.us.r-project.org"
options(repos = r)

knitr::opts_chunk$set(echo = TRUE)

library(tidyverse)
library(dplyr)

getwd()

```

##load in data
```{r}
candidates <- read_delim('input_data/Hanna_new_candidates.csv', delim = ';')
```

##remove Tc1-Mariners and cons, we are not interested in these for Experiment 6
```{r}
#smallds <-  candidates[1:10,]
#smallds <- as.data.frame(smallds)
#class(smallds)
candidates <- candidates[!grepl("DTT",candidates$TE_short),]
candidates <- candidates[!grepl("cons", candidates$EVE),]
```

##making sure that all the TEs are over 100bp long
```{r}
#smallds <- candidates[1:10,]

candidates <- candidates %>% mutate(TE_length=TE_end-TE_start)
view(candidates) # this includes TFBSs, if you continue you will just get the TEs
save(candidates, file = "results_folder/nonMariners_info.Rdata")

genes <- candidates[, -1:-12]
save(genes, file = "results_folder/nonMariner_GeneIDs")
```

#TEs list
```{r}
Order <- candidates[,19:24]
Order <- unique(Order)
Order

names(Order) <- NULL

save(Order, file="results_folder/nonMariner_TE_list.RData")

#saveRDS(Order, file = "../../Output_data/newCandidates.rds")
#Getting sequences using BEDTools versjon BEDTools/2.29.2-GCC-9.3.0
#bedtools getfasta -fi '../Salmon_genome.chr.fa -bed seqs.css >seqs.fa fra /mnt/SCRATCH/oymo/hannating/newseqs.'
```

0307 - probably LINE2 element
DTX 0849 Piggyback maybe
omd 242 - probably a SINE, SINEs can be 50bp long, are non-automatic, TEs can change categorization
