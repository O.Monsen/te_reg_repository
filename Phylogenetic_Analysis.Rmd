---
title: "Phylogenetic_Analysis"
output: 
  html_document: 
    toc: yes
    toc_float: yes
editor_options: 
  chunk_output_type: console
---


```{r}
r = getOption("repos")
r["CRAN"] = "http://cran.us.r-project.org"
options(repos = r)
```


```{r setup, include=FALSE}

knitr::opts_chunk$set(error = TRUE, echo = TRUE)

packages <- c("plotly", "ade4", "DT", "DECIPHER", "tidyverse", "dplyr", "rstudioapi", "BiocManager", "Rsamtools", "ape")

package.check <- lapply(
  packages, 
  FUN = function(x) {
    if (!require(x, character.only = TRUE)) {
      install.packages(x, dependencies = TRUE)
      library(x, character.only = TRUE)
    }
  }
)                          
```

####Set working directory to document directory
```{r}
setwd(dirname(getActiveDocumentContext()$path))       
getwd()
```

## Data Management{.tabset}
### Fasta Extraction 
####Loading in TE data
The TE data with all the variables is loaded into the program. This file is originally called up but we rename it data. Then we remove the duplicate file up. 
```{r}
load('input_data/Tc1_Mariners_phylogenetic_analysis/up.mariner.RData')
data <- up
```

####Grouping the TEs by Chromosome
The TEs are grouped according to which chromosome they are located on and by their long name. 
```{r}
data %>% group_by(TE_chrom) %>% summarize(TE_long)
```

####Renaming the TEs and Writing a New Bedfile
The TEs are renamed using a conversion file. The name is changed from the SsalEle format to XXX. First the file is read in. Then the conversion file is used for writing a new bedfile with the renamed TEs. 
```{r}
conversion <- read_tsv('input_data/Tc1_Mariners_phylogenetic_analysis/chromosome_name_conversion.txt', col_names = F)


data$TE_chrom <- conversion$X3[match(data$TE_chrom, conversion$X1)]
data %>% select('TE_chrom', 'TE_start', 'TE_stop', 'TE_long', 'TFBS_id', 'TFBS_chrom', 'TFBS_start', 'TFBS_end') %>% write_tsv(file = 'results_folder/Phylogenetic_analysis_output/TE_CREs_promoters.bed', col_names = F)
```

run: bedtools getfasta [OPTIONS] -fi <input FASTA> -bed <BED/GFF/VCF> through system command
```{r}

system('bedtools getfasta -fi input_data/Tc1_Mariners_phylogenetic_analysis/GCF_000233375.1_ICSASG_v2_genomic.fna  -bed results_folder/Phylogenetic_analysis_output/TE_CREs_promoters.bed -fo results_folder/Phylogenetic_analysis_output/TE_TFBS.fa')
```


####Combine Consensus and CRE_Fasta Files
Now the Consensus file will be combined with the newly created CRE file. 
First seqinr is installed. Remember that this masks count from dplyr. 
```{r}
library(seqinr)
```

Then the created TFBS file will be given a name.
```{r}
TE_TFBS_fasta <- read.fasta('results_folder/Phylogenetic_analysis_output/TE_TFBS.fa', as.string = T)
```

Any duplicate sequences were removed. 
```{r}
TE_TFBS_fasta <- TE_TFBS_fasta[unique(names(TE_TFBS_fasta))]
```

Then the consensus sequences are read in. 
```{r}
consensus_fasta <- read.fasta('input_data/Tc1_Mariners_phylogenetic_analysis/blastedconsensuses.fa', as.string = T)
```

Finally the TE file and the consensus file are combined
```{r}
combined_fasta <- c(TE_TFBS_fasta, consensus_fasta)
combined_fasta
```

Then the sequences that are shorter than 100 base pairs are removed. 
```{r}
all_fastas <- combined_fasta[getLength(combined_fasta) > 100]
head(all_fastas)
```

Then a new fasta file is written that contains the specified sequences from all_fastas
```{r}
write.fasta(all_fastas, names = gsub(':', '_', names(all_fastas)),
            file.out = 'results_folder/Phylogenetic_analysis_output/all_TEs.fa')
```

## Plotting a Phylogenetic Tree {.tabset}
###Aligning and Plotting Sequences in a Tree
####Aligning the Sequences

The sequnces are aligned and adjusted for reverse complements. This was done through Mafft. 
```{r}
system('mafft --adjustdirection --retree 1 results_folder/Phylogenetic_analysis_output/all_TEs.fa > results_folder/Phylogenetic_analysis_output/all_TEs_retree1.ali' )
```

#### Making a Phylogenetic Tree
First a Phylogenetic Tree is constructed from the alignments. This is done by using FastTree. 
```{r}
system('input_data/Tc1_Mariners_phylogenetic_analysis/FastTree -nt results_folder/Phylogenetic_analysis_output/all_TEs_retree1.ali > results_folder/Phylogenetic_analysis_output/all_TEs_retree1.tree')
```

#### Reading and Plotting the Tree
First the tree is read as "tree"
```{r}
install.packages("ggtree",
                 repos = "https://www.bioconductor.org/packages/devel/bioc",
                 type = "source")
library("ggtree")

tree <- read.tree('results_folder/Phylogenetic_analysis_output/all_TEs_retree1.tree')

```

#### Plotting the tree using ggtree
```{r}

NC <- grep('NC', tree$tip.label)
NC

pdf(file = 'results_folder/Phylogenetic_analysis_output/Phylogenetic_tree.pdf', height = 17, width = 17)

  ggtree(tree, branch.length='none', layout='circular') + 
    geom_treescale() + 
    geom_tiplab(size = 2, aes(color=grepl("DTT", label))) +
    scale_color_manual(name = 'legend', values=c("red", "black"), labels=c("Test TEs", "consensus TE")) +
    #geom_text(aes(label=node), hjust=-.1, ) +
    geom_hilight(node=240, fill="purple", alpha = 0.2) +
    geom_hilight(node=207, fill="gold", alpha = 0.2) +
    geom_hilight(node=171, fill="brown", alpha = 0.2) +
    geom_hilight(node=173, fill="red", alpha = 0.2) +
    geom_hilight(node=199, fill="green", alpha = 0.2) +
    geom_hilight(node=278, fill="blue", alpha = 0.2) +
    theme(plot.title = element_text(size=22), legend.position = c(0.08, 0.92), legend.key.size = unit(1, 'cm'), 
    legend.title = element_text(size=17), legend.text = element_text(size=15)) #+
    #ggtitle("Phylogenetic Tree - Transposable Elements (Cis-reglulatory elements) aligned with consensus sequences")

dev.off()

```


## TE-CREs Alignments {.tabset}
#### Making Pairwise Alignments in the Tree
First we find the nearest consensus sequences.
```{r}
cp <- cophenetic.phylo(tree)
grep("NC_027308.1_24002910-24004524", colnames(cp))

cp <- cp[grep('NC', rownames(cp)),-grep('NC', rownames(cp))]
idx_nearest <- apply(cp, 1, which.min)
idx_nearest

nearest_consensus = data.frame(TFBS_TE = grep('NC', rownames(cp), value = T), consensus_TE = colnames(cp)[idx_nearest], stringsAsFactors = F)

nearest_consensus$consensus_TE <- gsub('_R_', '', nearest_consensus$consensus_TE)
nearest_consensus$TFBS_TE <- gsub('_R_', '', nearest_consensus$TFBS_TE)
nearest_consensus

fastas <-   read.fasta('results_folder/Phylogenetic_analysis_output/all_TEs.fa', as.string = T)
fastas

fasta_out <- c()
fasta_out

for(i in 1:nrow(nearest_consensus)){
  print(i)
  fasta_out <- paste0('results_folder/Phylogenetic_analysis_output/', names(fastas[nearest_consensus$TFBS_TE[i]]), '_vs_', names(fastas[nearest_consensus$consensus_TE[i]]), '.fasta')
  
  fasta_out <- gsub('#|DNA\\/', '_', fasta_out)
  
  write.fasta(c(fastas[nearest_consensus$TFBS_TE[i]], fastas[nearest_consensus$consensus_TE[i]]), names = names(c(fastas[nearest_consensus$TFBS_TE[i]], fastas[nearest_consensus$consensus_TE[i]])), file.out = fasta_out)
  
  system(paste0('mafft --adjustdirection --auto ', fasta_out, ' > ', gsub('\\.fasta', '\\.ali', fasta_out )))
  
}

fasta_out
```
